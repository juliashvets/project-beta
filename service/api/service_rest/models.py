from django.db import models

# Create your models here.

#Technician Model

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    
class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.IntegerField()

class Appointment(models.Model):
    date_time = models.DateTimeField(default="2024-02-06 12:30:00")
    reason = models.CharField(max_length=100, default='oil change')
    status = models.CharField(max_length=100)
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=100)
    
    technician = models.ForeignKey(
        Technician,
        related_name='appointments',
        on_delete=models.CASCADE,
    )

    






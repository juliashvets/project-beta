import React, { useEffect, useState } from 'react';

import History from './ServiceHistoryList'



function HistoryForm() {
    const [vin, setVin] = useState('');
    const [submitted, setSubmitted] = useState(false);
    

    const handleVinChange = (e) => {
        setVin(e.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('vin:', vin)
        setSubmitted(true);
    };

    return (
        <div className="container mt-5">
            <h1 className="mb-4">Service History</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="vin" className="form-label">Enter VIN</label>
                    <input
                        type="text"
                        value={vin}
                        onChange={handleVinChange}
                        className="form-control"
                        id="vin"
                        placeholder="VIN"
                        required
                    />
                </div>
                <button type="submit" className="btn btn-primary">Show Service History</button>
            </form>
            {submitted && <History vin={vin} />}
        </div>
    );




}

export default HistoryForm;

    

   
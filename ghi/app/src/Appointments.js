import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function Appointments() {
    const [appointments, setAppointments] = useState([]);
    const [inventoryData, setInventoryData] = useState([]);
    const fetchAppointments = async () => {
        try {
            const url = 'http://localhost:8080/api/appointments';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments);
            } else {
                throw new Error('failed to fetch');
            }
        } catch (error) {
            console.error('error', error);
        }
    };

    const fetchInventoryData = async () => {
        try {
            const url = 'http://localhost:8100/api/automobiles/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setInventoryData(data.autos);
            } else {
                throw new Error('Failed to fetch inventory data');
            }
        } catch (error) {
            console.error('Error fetching inventory data');
        }
    }

    useEffect(() => {
        fetchAppointments();
        fetchInventoryData();
    }, []);

    const isVipCustomer = (vin) => {
        return inventoryData.some(auto => auto.vin === vin);
    }

    const handleCompleteAppointment = (id) => {
        const updatedAppointments = appointments.filter(appointment => appointment.id !== id);
        setAppointments(updatedAppointments);
    }

    const handleCancelAppointment = async (id) => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}/`, {
                method: 'DELETE',
            });

            if (!response.ok) {
                throw new Error('Failed to cancel appointment');
            }

            const updatedAppointments = appointments.filter(appointment => appointment.id !== id);
            setAppointments(updatedAppointments);

        } catch (error) {
            console.error('Error cancelling appointment:', error);
        }
    };

    return (
        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer Name</th>
                    <th>Date and Time</th>
                    <th>Assigned Technician Name</th>
                    <th>Reason for Service</th>
                    <th>VIP?</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return (
                        <tr key={ appointment.id }>
                            <td>{ appointment.vin }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td>{ appointment.technician.id }</td>
                            <td>{ appointment.reason }</td>
                            <td>{ isVipCustomer(appointment.vin) ? 'Yes' : 'No' }</td>
                            <td><button className='btn btn-success' style={{ padding: '5px 5px', fontSize: '14px', borderRadius: '5px' }} onClick={() => handleCompleteAppointment(appointment.id)}>Complete Appointment</button></td>
                            <td><button className='btn btn-danger' style={{ padding: '5px 5px', fontSize: '14px', borderRadius: '5px' }} onClick={() => handleCancelAppointment(appointment.id)}>Cancel Appointment</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default Appointments;

import React, { useState, useEffect } from 'react';

function SalespersonHistoryForm() {
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [filteredSales, setFilteredSales] = useState([]);

    const fetchSalespeople = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespeopleUrl);
        if (response.ok) {
          const data = await response.json();
          setSalespeople(data.salespeople);
        }
    }
    useEffect(() => {
            fetchSalespeople();
    }, []);


    const fetchFilteredSales = async () => {
        const salesUrl = `http://localhost:8090/api/sales/salesperson/${salesperson}/`;
        const response = await fetch(salesUrl);
        if (response.ok) {
            const data = await response.json();
            setFilteredSales(data.sales);
        }
    }

    useEffect(() => {
        if (salesperson) {
            fetchFilteredSales();
        }
    }, [salesperson]);

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }


    return (
      <div>
        <h1> Salesperson History </h1>
        <div className="mb-3">
            <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                <option value="">Select a salesperson</option>
                {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>{`${salesperson.first_name} ${salesperson.last_name}`}</option>
                    )
                })}
            </select>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson Name</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {filteredSales.map(sale => {
             return (
              <tr key={sale.id}>
                <td>{ `${sale.salesperson.first_name} ${sale.salesperson.last_name}` }</td>
                <td>{ `${sale.customer.first_name} ${sale.customer.last_name}` }</td>
                <td>{ sale.automobile.vin }</td>
                <td>{ sale.price }</td>
              </tr>
             )
            })}
          </tbody>
        </table>
      </div>
    )
}

export default SalespersonHistoryForm;

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Appointments from './Appointments';
import AppointmentForm from './AppointmentForm';
import TechnicianForm from './TechnicianForm';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import HistoryForm from './ServiceHistoryForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalespeopleList from './SalespeopleList';
import SalespersonForm from './SalespersonForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalespersonHistoryForm from './SalespersonHistoryForm';
import AutomobilesList from './AutomobilesList';
import AutomobileForm from './AutomobileForm';

function App(props) {
  if (props.appointments === undefined) {
    return null
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/appointments" element={<Appointments  />} />
          <Route path="/appointments/new" element={<AppointmentForm  />} />
          <Route path="/technicians/new" element={<TechnicianForm  />} />
          <Route path="/manufacturers" element={<ManufacturerList  />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm  />} />
          <Route path="/models" element={<VehicleModelList  />} />
          <Route path="/models/new" element={<VehicleModelForm  />} />
          <Route path="/history" element={<HistoryForm  />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/salespeople/" element={<SalespeopleList />} />
          <Route path="/salespeople/new" element={<SalespersonForm />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales/new" element={<SalesForm />} />
          <Route path="/sales/history" element={<SalespersonHistoryForm />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

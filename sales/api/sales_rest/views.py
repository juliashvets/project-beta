import json
from .models import AutomobileVO, Salesperson, Customer, Sale
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from decimal import Decimal

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "automobile",
        "salesperson",
        "customer",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }
    def default(self, obj):
        if isinstance(obj, Decimal):
            return '$' + str(obj)
        return super().default(obj)

@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def delete_salesperson(request, id=None):
    if request.method == "DELETE":
        deleted, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": deleted > 0})


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content= json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def delete_customer(request, id=None):
    if request.method == "DELETE":
        deleted, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": deleted > 0})


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        price_str = content['price']
        try:
            price = Decimal(price_str.replace('$', '').strip())
        except:
            return JsonResponse(
                {"message": "Invalid price format"},
                status=400,
            )
        content['price'] = price
        try:
            salesperson_id = content['salesperson']
            salesperson = Salesperson.objects.get(pk=salesperson_id)
            content['salesperson'] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson not found"},
                status=400,
            )
        try:
            customer_id = content['customer']
            customer = Customer.objects.get(pk=customer_id)
            content['customer'] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer not found"},
                status=400,
            )
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin = automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile not found"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def delete_sale(request, id=None):
    if request.method == "DELETE":
        deleted, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": deleted > 0})

@require_http_methods(["GET"])
def filter_sales(request, salesperson_id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(pk=salesperson_id)
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson not found"},
                status=400,
            )

        sales = Sale.objects.filter(salesperson=salesperson)
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
